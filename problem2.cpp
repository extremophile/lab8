/***
FN:F79454
PID:2
GID:1
*/

#include <iostream>

using namespace std;


void do_the_thing(const string &str)
{
for (int i = str.length() - 1; i >= 0; i--)
{
cout << str[i];
}
}

int main()
{
cout << "Enter a string: " << endl;
string str;

while(cin >> str)
{
if(str == "exit")
{
break;
}
else
{
do_the_thing(str);
return 0;
}
}
}
